#!/bin/bash

for city in $1/*; do
  cd $2/ && mkdir "${city##*/}"
  for subcategory in "$city"/*; do
    cd $2/"${city##*/}" && mkdir "${subcategory##*/}"
    for journey in "$subcategory"/*.gpx; do
      cd $2/"${city##*/}"/"${subcategory##*/}" && python3 $3/journey-parser/journey-parser.py "$journey"
    done

  done
done